package plugins.stef.library;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * EJML (Efficient Java Matrix Library) for Icy
 * 
 * @author Stephane Dallongeville
 */
public class EJMLPlugin extends Plugin implements PluginLibrary
{
    //
}
